import React from "react";
import SignUp from "./pages/SignUp";
import { Provider } from "react-redux";
import Login from "./pages/Login";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Hompage from "./pages/Hompage";
import store from "./redux/store";
import Transaction from "./pages/Transaction";
import ExpenseDetails from "./pages/ExpenseDetails";

import IncomeDetails from "./pages/IncomeDetails";
import Errorpage from "./pages/Errorpage";
import Expenses from "./components/Expenses";
import MasterLayout from "./layout/MasterLayout";
import ProtectedRoute from "./components/ProtectedRoute";
import Dashboard from "./pages/Dashboard";
const App: React.FC = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<MasterLayout />}>
            <Route index element={<Hompage />} />
            <Route path="signup" element={<SignUp />} />
            <Route path="login" element={<Login />} />
            <Route element={<ProtectedRoute />}>
              <Route path="dashboard" element={<Dashboard />} />
              <Route path="income" element={<IncomeDetails />} />
              <Route path="transaction" element={<Transaction />} />
              <Route path="expense" element={<ExpenseDetails />} />
            </Route>
          </Route>
          <Route path="*" element={<Errorpage/>}/>
        </Routes>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
