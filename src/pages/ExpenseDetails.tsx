import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import {
  Table,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  Typography,
  Container,
  InputLabel,
  Select,
  MenuItem,
  Grid,
  FormControl,
  Button,
  FormControlLabel,
  Checkbox,
  TextField, // Import TextField for date picker
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useDispatch } from "react-redux";

import { useFormik } from "formik";
import * as yup from "yup";
import { afterExpenceIncome } from "../redux/features/income/incomeSlice";
import {
  deleteExpense,
  editExpense,
  showExpenseForm,
  totalExpenseInfo,
} from "../redux/features/expense/expenseSlice";
import "../assets/css/incomeDetails.css";
import { useNavigate } from "react-router";
import {
  ShoppingCart as GroceriesIcon,
  Commute as TransportationIcon,
  LocalActivity as EntertainmentIcon,
  FlashOn as UtilitiesIcon,
  LocalHospital as HealthcareIcon,
  MoreHoriz as OthersIcon,
} from "@mui/icons-material";
import "../assets/css/expensedetails.css";
import { red } from "@mui/material/colors";
import CustomTextField from "../components/common/CustomTextField";
interface value {
  category : string
  customCategory: string
  expense: string
  date: string
  customNote: string
}
const categories: string[] = [
  "Groceries",
  "Transportation",
  "Entertainment",
  "Utilities",
  "Healthcare",
  "Others",
];

const ExpenseDetails: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const isDialogOpen = useSelector(
    (state: RootState) => state.expense.isDialogOpen
  );
  const expenseData = useSelector((state: RootState) => state.expense.expense);
  const [data, setData] = useState(expenseData);
  const [expense, setExpense] = useState({}) as any;
  // const [isDialogOpen, setDialogOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedCategory, setSelectedCategory] = useState<string | "">("");
  const [selectedDate, setSelectedDate] = useState("");

  const [sortByDate, setSortByDate] = useState<boolean>(false);

  useEffect(() => {
    setData(expenseData);
    if (expense) {
      formik.setValues(expense);
    }
  }, [expenseData]);

  const handleEdit = (expense: any, index: number) => {
    setExpense({ ...expense, index });
    formik.setValues({
      category: categories?.includes(expense?.categories)
        ? expense?.categories
        : "Others",
      customCategory: categories?.includes(expense?.categories)
        ? ""
        : expense?.categories,
      expense: expense?.expense || "",
      date: expense?.date || "",
      customNote: expense?.customNote || "",
    });
    dispatch(showExpenseForm(true));
    // setDialogOpen(true);
  };

  const handleDelete = (index: number) => {
    dispatch(deleteExpense(index));
  };

  const handleCloseDialog = () => {
    setSelectedCategory(""); // Reset the selected category
    setSelectedDate(""); // Reset the selected date
    // setDialogOpen(false);
    dispatch(showExpenseForm(false));
  };

  const validationSchema = yup.object({
    // category: yup.string().required("Please select category"),
    // customCategory: yup.string().required("Please select custom category"),
    expense: yup
    .number()
    .required("Expense is required")
    .positive("Income must be a positive number"),
    // date: yup.string().required("Please select date"),
  });

  const formik = useFormik({
    initialValues: {
      category: "",
      customCategory: "",
      expense: "",
      date: "",
      customNote: "",
    },
    
    onSubmit: (values: value) => {
      const updatedExpense = {
        expense: values?.expense,
        date: values?.date,
        categories:
          values?.category === "Others"
            ? values?.customCategory
            : values?.category,
        customNote: values?.customNote ? values?.customNote : "--",
      };

      dispatch(editExpense({ index: expense.index, updatedExpense }));
      dispatch(afterExpenceIncome(updatedExpense.expense));
      dispatch(totalExpenseInfo(updatedExpense.expense));
      dispatch(showExpenseForm(false));
    },
    validationSchema
 
  });

  interface DataItem {
    date: string;
  }
  const filteredData = data.filter(
    (expense: any) =>
      expense?.categories?.toLowerCase().includes(searchQuery.toLowerCase()) &&
      ((selectedCategory === "Others" &&
        categories?.indexOf(expense.categories) === -1) ||
        selectedCategory === "" ||
        expense?.categories === selectedCategory) &&
      (selectedDate === "" || expense?.date === selectedDate)
  );
  if (sortByDate) {
    filteredData.sort(
      (frist: DataItem, second: DataItem) =>
        new Date(frist.date).getTime() - new Date(second.date).getTime()
    );
  }

  const handleResetDateFilter = () => {
    setSelectedDate("");
  };

  return (
    <div className="containerMain">
<Button></Button>
    <>
          
          <Paper elevation={10}>
            <Dialog open={isDialogOpen} onClose={handleCloseDialog}>
              <form onSubmit={formik.handleSubmit}>
                <DialogTitle>Edit Expense</DialogTitle>
                <DialogContent>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <FormControl fullWidth margin="normal">
                        <InputLabel>Category</InputLabel>
                        <Select
                          className="select-dropdown"
                          name="category"
                          value={formik.values.category}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        >
                          {categories.map((cat) => (
                            <MenuItem key={cat} value={cat} >
                            {cat === "Groceries" && <GroceriesIcon className="icon" />} 
                            {cat === "Transportation" && <TransportationIcon  className="icon"/>}
                            {cat === "Entertainment" && <EntertainmentIcon  className="icon"/>}
                            {cat === "Utilities" && <UtilitiesIcon  className="icon"/>}
                            {cat === "Healthcare" && <HealthcareIcon  className="icon"/>}
                            {cat === "Others" && <OthersIcon />}
                            <span>{cat}</span>
                          </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Grid>
                    {(expense.categories === "Others" ||
                      formik.values.category === "Others") && (
                      <Grid item xs={12}>
                        <CustomTextField
                          className=" "
                          label="Custom Category"
                          name="customCategory"
                          type="text"
                          value={formik.values.customCategory}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                        />
                      </Grid>
                    )}
                    <Grid item xs={12}>
                      <CustomTextField
                        label="Expense"
                        type="number"
                        name="expense"
                        value={formik.values.expense}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                      />
                    </Grid>{" "}
                    <Grid item xs={12}>
                      <CustomTextField
                        label="Date"
                        type="date"
                        name="date"
                        value={formik.values.date}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <CustomTextField
                        name="customNote"
                        label="Custom Note"
                        value={formik.values.customNote}
                        onChange={formik.handleChange}
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    </Grid>
                  </Grid>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleCloseDialog} color="primary">
                    Cancel
                  </Button>
                  <Button type="submit" color="primary">
                    Save
                  </Button>
                </DialogActions>
              </form>
            </Dialog>
          </Paper>
          <Container maxWidth='lg'>
          <Typography variant="h4" gutterBottom>
            Expense Details
          </Typography>
          </Container>
          <Container maxWidth="xl" className="expensedata-container">
          <Grid container  alignItems="center" direction="column" justifyContent="center" >
            <Grid container  alignItems="center"  justifyContent="space-between"  >
            <Grid item xs={12} sm={6} md={3}>   
            <FormControl fullWidth margin="normal">
              <InputLabel>Category Filter</InputLabel>
              <Select
                name="categoryFilter"
                value={selectedCategory}
                onChange={(e) => setSelectedCategory(e.target.value as string)}
              >
                <MenuItem value="">All Categories</MenuItem>
                {categories.map((cat) => (
                            <MenuItem key={cat} value={cat} >
                              {cat === "Groceries" && <GroceriesIcon className="icon" />} 
                              {cat === "Transportation" && <TransportationIcon  className="icon"/>}
                              {cat === "Entertainment" && <EntertainmentIcon  className="icon"/>}
                              {cat === "Utilities" && <UtilitiesIcon  className="icon"/>}
                              {cat === "Healthcare" && <HealthcareIcon  className="icon"/>}
                              {cat === "Others" && <OthersIcon />}
                              <span>{cat}</span>
                            </MenuItem>
                ))}
              </Select>
            </FormControl>
            {selectedCategory && (
    
            <Button onClick={() => setSelectedCategory("")} variant="contained"  className='resetButoon'>Reset Category</Button>
            )}
            </Grid>
            <Grid item xs={12} sm={6} md={3}>  
            <FormControl fullWidth margin="normal">
             
              <TextField
                type="date"
                value={selectedDate}
                onChange={(e) => setSelectedDate(e.target.value)}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </FormControl>
            {selectedDate && (
    
              <Button onClick={handleResetDateFilter} variant="contained"  className='resetButoon'>Reset Date</Button>
            )}
            </Grid>
            <Grid item xs={12} sm={6} md={3}>  
            <FormControl fullWidth margin="normal">
            <CustomTextField
              label="Search"
              type="search"
              name="search"
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
              
            />
            </FormControl>
            {searchQuery && 
              <Button onClick={() => setSearchQuery("")}  variant="contained"  className='resetButoon'>Reset Search</Button>
            }
            </Grid>
            </Grid>
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow className="tableHeader">
                      <TableCell className='tableCell'>
                      <FormControlLabel
                     label="Sort by Date"
                            control={
                                <Checkbox
                               className='input-checkbox'
                                checked={sortByDate}
                                onChange={(e) => setSortByDate(e.target.checked)}
                                color="success"
                                />
                            }
                            />
                      </TableCell>
                      <TableCell className='tableCell'>Categories</TableCell>
                      <TableCell className='tableCell'>Expense Amount</TableCell>
                      <TableCell className='tableCell'>Custom Note</TableCell>
                      <TableCell className='tableCell'>Edit</TableCell>
                      <TableCell className='tableCell'>Delete</TableCell>
                    </TableRow>
                  </TableHead>
                  {filteredData.length === 0 ?
                (
                  <TableRow className="tableRow">
                  <TableCell colSpan={6} className="h3404page" align="center">
                  No Data Found For Search
                </TableCell>
                </TableRow>
                 ):(
                  <TableBody>
                    {filteredData.map((expense: any, index: number) => (
                      <TableRow key={index}  className="tableRow">
                        <TableCell>{expense.date}</TableCell>
                        <TableCell>{expense.categories}</TableCell>
                        <TableCell>{expense.expense}</TableCell>
                        <TableCell>{expense.customNote}</TableCell>
                        <TableCell>
                          <IconButton
                            color="primary"
                            onClick={() => handleEdit(expense, index)}
                          >
                            <EditIcon />
                          </IconButton>
                        </TableCell>
                        <TableCell>
                          <IconButton
                            color="secondary"
                            onClick={() => handleDelete(index)}
                          >
                            <DeleteIcon />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                )}
                </Table>
              </TableContainer>
              
            
          </Grid>
          </Container>
          </>
        
        </div>
  );
};

export default ExpenseDetails;
