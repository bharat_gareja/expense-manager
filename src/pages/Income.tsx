import React, { useState } from "react";
import { useDispatch } from "react-redux";
import "../../src/assets/css/income.css";
import {
  Button,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Typography,
  Paper,
  Grid,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import {
  addIncome,
  showIncomeForm,
  totalIncomeIfo,
} from "../redux/features/income/incomeSlice";
import { useFormik } from "formik";
import * as Yup from "yup";
// import CustomTextField from "../components/common/CustomTextFeild";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import CustomTextField from "../components/common/CustomTextField";
import {
  AddCircleOutline as SalaryIcon,
  MonetizationOn as StockIcon,
  FitnessCenter as GymTrainerIcon,
  Work as FreelancingIcon,
  MoreHoriz as OthersIcon,
} from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import { red } from "@mui/material/colors";
// import CustomTextField from "../components/common/CustomTextFeild";
// import CustomTextField from "../components/common/CustomTextField";
import { addTransaction } from "../redux/features/transaction/transactionSlice";

const validationSchema = Yup.object({
  incomeType: Yup.string().required("Income type is required"),
  customIncomeType: Yup.string().test(
    "customTypeRequired",
    'Custom Income Type is required when "Other" is selected',
    function (value) {
      if (this.parent.incomeType === "Others") {
        return !!value;
      }
      return true;
    }
  ),
  amount: Yup.number()
    .required("Amount is required")
    .positive("Income must be a positive number"),
  customNote: Yup.string(),
  date: Yup.date().required("Date is required"),
});

const Income: React.FC = () => {
  const dispatch = useDispatch();
  const isDialogOpen = useSelector(
    (state: RootState) => state.income.isDialogOpen
  );
  const [incomeTypes, setIncomeTypes] = useState<string[]>([
    "salary",
    "stock",
    "gymtrainer",
    "freelancing",
    "Others",
  ]);

  

  const formik = useFormik({
    initialValues: {
      incomeType: "",
      customIncomeType: "",
      amount: "",
      customNote: "",
      date: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values, action) => {
      const incomeData = {
        date: values.date,
        customNote: values.customNote ? values.customNote : "--", // Change customnote to customNote
        amount: values.amount,
        incomeType:
          values.incomeType === "Others"
            ? values.customIncomeType
            : values.incomeType, // Change incomeTypes to incomeType
      };
      dispatch(addIncome(incomeData));
      dispatch(addTransaction({ type: "income", data: incomeData }));

      dispatch(totalIncomeIfo(incomeData.amount));
      console.log(incomeData);
      dispatch(showIncomeForm(false));
      action.resetForm();
    },
  });
  

  const handleCloseDialog = () => {
    dispatch(showIncomeForm(false));
    formik.resetForm();
  };
  return (
    <Container className="Income">
      <Paper elevation={10}>
        <Dialog open={isDialogOpen} onClose={handleCloseDialog}>
          <form onSubmit={formik.handleSubmit}>
            <DialogTitle>Add Income</DialogTitle>
            <DialogContent>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <FormControl fullWidth margin="normal">
                    <InputLabel>Income Type</InputLabel>
                    <Select
                      name="incomeType"
                      value={formik.values.incomeType}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                    >
                      {incomeTypes.map((type) => (
                        <MenuItem
                        key={type}
                        value={type}
                        className="menu-item"
                      >
                        {type === "salary" && <SalaryIcon className="icon"  />}
                        {type === "stock" && <StockIcon className="icon" />}
                        {type === "gymtrainer" && <GymTrainerIcon className="icon" />}
                        {type === "freelancing" && <FreelancingIcon className="icon" />}
                        {type === "Others" && <OthersIcon className="icon" />}
                        <span >{type}</span>
                      </MenuItem>
                      ))}
                    </Select>
                    {formik.errors.incomeType && formik.touched.incomeType && (
                      <Typography color={red[500]}>
                        {formik.errors.incomeType}
                      </Typography>
                    )}
                  </FormControl>
              
                </Grid>

                {formik.values.incomeType === "Others" && (
                  <Grid item xs={12}>
                    <CustomTextField
                      name="customIncomeType"
                      label="Custom Income Type"
                      type="text"
                      value={formik.values.customIncomeType}
                      onChange={formik.handleChange}
                      errorText={
                        formik.touched.customIncomeType &&
                        formik.errors.customIncomeType
                      }
                    />
                  </Grid>
                )}
                <Grid item xs={12}>
                  <CustomTextField
                    className="amount"
                    name="amount"
                    label="Amount"
                    type="number"
                    value={formik.values.amount}
                    onChange={formik.handleChange}
                    errorText={formik.touched.amount && formik.errors.amount}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <CustomTextField
                    name="customNote"
                    label="Custom Note"
                    value={formik.values.customNote}
                    onChange={formik.handleChange}
                    errorText={
                      formik.touched.customNote && formik.errors.customNote
                    }
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <CustomTextField
                    name="date"
                    label="Date"
                    type="date"
                    value={formik.values.date}
                    onChange={formik.handleChange}
                    errorText={formik.touched.date && formik.errors.date}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseDialog} color="primary">
                Cancel
              </Button>
              <Button variant="contained" color="primary" type="submit">
                Add
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </Paper>
    </Container>
  );
};

export default Income;
