import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  Grid,
  Typography,
  Button,
  Tooltip,
  FormControlLabel,
  Checkbox,
  Container,
  Link,
} from "@mui/material";

import { styled } from "@mui/material/styles";
import { FormControl,InputLabel, MenuItem,Select } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { RootState } from '../redux/store';
import { useDispatch, useSelector } from 'react-redux';
import { deleteIncome } from '../redux/features/income/incomeSlice';
import '../assets/css/incomeDetails.css';
import {
  AddCircleOutline as SalaryIcon,
  MonetizationOn as StockIcon,
  FitnessCenter as GymTrainerIcon,
  Work as FreelancingIcon,
  MoreHoriz as OthersIcon,
} from '@mui/icons-material';


import IncomeEditModal from "../components/IncomeEditModal";
import CustomTextField from "../components/common/CustomTextField";
import { useNavigate } from "react-router-dom";
interface Income {
  incomeType: string;
  amount: string;
  date: string;
  customNote: string;
  customIncomeType?: string;
}

//use reduce method to total all array amount
const getTotalIncome = (incomes: Income[]) => {
  return incomes.reduce(
    (total: number, income) => total + parseInt(income.amount),
    0
  );
};
interface DataItem {
  date: string;
}
const incomeCategory: string[] = [
  "salary",
  "stock",
  "gymtrainer",
  "freelancing",
  "Others",
];

const IncomeDetails: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const incomes = useSelector((state: RootState) => state.income.incomes);
  //modal state
  const [selectedIncome, setSelectedIncome] = useState<Income | null>(null);
  const [selectedIncomeIndex, setSelectedIncomeIndex] = useState<number | null>(
    null
  );
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);

  //filter data state
  const [selectedCategory, setSelectedCategory] = useState<string | null>(null);
  const [selectedDate, setSelectedDate] = useState<string | null>(null);
  const [searchTextData, setSearchTextData] = useState<string | "">("");
  const [sortByDate, setSortByDate] = useState<boolean>(false);

  const handleEditClick = (income: Income, index: number) => {
    setIsEditModalOpen(true);
    setSelectedIncome(income);
    setSelectedIncomeIndex(index);
  };

  const handleEditModalClose = () => {
    setIsEditModalOpen(false);
    setSelectedIncome(null);
    setSelectedIncomeIndex(null);
  };

  // Function to filter incomes based on selected category
  let filterData = [...incomes];
  const filteredIncomes = (
    category: string | null,
    date: string | null,
    querystring: string | null,
    sortByDate: boolean
  ) => {
    //filter category

    if (category) {
      if (category === "Others") {
        //when user selected Other than dispay customCategory
        filterData = filterData.filter(
          (income: Income) =>
            !incomeCategory
              .filter((cat) => cat !== "Others")
              .includes(income.incomeType)
        );
      } else {
        filterData = filterData.filter(
          (income: Income) => income.incomeType === category
        );
      }
    }
    //filter date 
    if (date) {
      filterData = filterData.filter((income: Income) => income.date === date);
    }
    //filter string text
    if (querystring) {
      filterData = filterData.filter((income: Income) =>
        income.incomeType.toLowerCase().includes(searchTextData.toLowerCase())
      );
    }
    if (sortByDate) {
      filterData.sort(
        (current: DataItem, nextdate: DataItem) =>
          new Date(current.date).getTime() - new Date(nextdate.date).getTime()
      );
    }

    return filterData;
  };

  //total amount calculate
  const total = filteredIncomes(
    selectedCategory,
    selectedDate,
    searchTextData,
    sortByDate
  );
  const totalAmount: string = getTotalIncome(total).toString();

  return (
    <>
      <div className="containerMain">
        <Container maxWidth='lg'>
          <Typography
                mt={2}
                gutterBottom
                variant="h4"
                className="incomeTitle"
              >
                Income Details
              </Typography>
              </Container>
        <Grid
          container
          alignItems="center"
          direction="column"
          justifyContent="center"
        >
            <>
              <Container maxWidth="xl">
                <Grid
                  container
                  justifyContent="space-between"
                >
                  <Grid item xs={12} sm={6} md={3}>
                    <Grid container direction="row">
                      <FormControl fullWidth margin="normal">
                        <InputLabel id="incomeCategoryFilter" className="incomeCategorykabel">
                          Search By Category
                        </InputLabel>
                        <Select
                        className="incomeCategoryFilter"
                          name="incomeCategoryFilter"
                          value={selectedCategory || ""}
                          onChange={(e) =>
                            setSelectedCategory(e.target.value as string)
                          }
                        >
                          
                          <MenuItem value="">All Categories</MenuItem>
                          {incomeCategory.map((CategoryType) => (
                          <MenuItem key={CategoryType} value={CategoryType}>
                            {CategoryType === 'salary' && <SalaryIcon />}
                            {CategoryType === 'stock' && <StockIcon />}
                            {CategoryType === 'gymtrainer' && <GymTrainerIcon />}
                            {CategoryType === 'freelancing' && <FreelancingIcon />}
                            {CategoryType === 'Others' && <OthersIcon />}
                            <span style={{ marginLeft: '8px' }}>{CategoryType}</span>
                          </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                      {selectedCategory && (
                        <Button variant="contained"  className='resetButoon' onClick={() => setSelectedCategory(null)}>
                          Reset Category
                        </Button>
                      )}
                    </Grid>
                  </Grid>

                  <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth margin="normal">
                      <Grid container direction="row">
                        <CustomTextField
                          name="filterdate"
                          type="date"
                          onChange={(e) => setSelectedDate(e.target.value)}
                          value={selectedDate || ""}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        {selectedDate && (
                          <Button variant="contained"  className='resetButoon' onClick={() => setSelectedDate(null)}>
                            Clear Date
                          </Button>
                        )}
                      </Grid>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} sm={6} md={3}>
                    <FormControl fullWidth margin="dense">
                      <Grid container direction="row">
                        <CustomTextField
                          label="Search"
                          type="search"
                          name="searchBox"
                          value={searchTextData}
                          onChange={(e) => setSearchTextData(e.target.value)}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        {searchTextData && (
                          <Button variant="contained"  className='resetButoon' onClick={() => setSearchTextData("")}>
                            Clear Search
                          </Button>
                        )}
                      </Grid>
                    </FormControl>
                  </Grid>
                </Grid>
                <TableContainer component={Paper}>
                  <Table aria-label="income table">
                    <TableHead>
                      <TableRow className="tableHeader">
                        <TableCell className="tableCell" align="center">
                          Category
                        </TableCell>
                        <TableCell className="tableCell" align="center">
                          <FormControlLabel
                            label="Sort by Date"
                            control={
                              <Checkbox
                                className="input-checkbox"
                                checked={sortByDate}
                                onChange={(e) =>
                                  setSortByDate(e.target.checked)
                                }
                                color="success"
                              />
                            }
                          />
                        </TableCell>
                        <TableCell className="tableCell" align="center">
                          Custom note
                        </TableCell>
                        <TableCell className="tableCell" align="center">
                          Edit
                        </TableCell>
                        <TableCell className="tableCell" align="center">
                          Delete
                        </TableCell>
                        <TableCell className="tableCell" align="center">
                          Amount
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    {filterData.length === 0 ? (
                      <>
                      <TableRow className="tableRow">
                        <TableCell colSpan={6} className="h3404page" align="center">
                        No Data Found For Search
                      </TableCell>
                      </TableRow>
                      </>
                    ) : (
                      <TableBody>
                        {total.map((income: Income, index: number) => (
                          <TableRow key={index} className="tableRow">
                            <TableCell align="center">
                              {income.incomeType}
                            </TableCell>
                            <TableCell align="center">{income.date}</TableCell>
                            <TableCell align="center">
                              {income.customNote}
                            </TableCell>

                            <TableCell align="center">
                              <Tooltip title="Edit">
                                <IconButton
                                  aria-label="edit"
                                  color="primary"
                                  onClick={() => handleEditClick(income, index)}
                                >
                                  <EditIcon />
                                </IconButton>
                              </Tooltip>
                            </TableCell>
                            <TableCell align="center">
                              <Tooltip title="Delete">
                                <IconButton
                                  aria-label="delete"
                                  color="secondary"
                                  onClick={() => dispatch(deleteIncome({index,income}))}
                                >
                                  <DeleteIcon />
                                </IconButton>
                              </Tooltip>
                            </TableCell>
                            <TableCell align="center">
                              {income.amount}
                            </TableCell>
                          </TableRow>
                        ))}
                        {/* <TableRow>
                          <TableCell colSpan={5} align="right">
                            Total Amount
                          </TableCell>

                          <TableCell align="right">
                            <Typography
                              variant="body1"
                              align="center"
                              fontWeight="bold"
                            >
                              {totalAmount}
                            </Typography>
                          </TableCell>
                        </TableRow> */}
                      </TableBody>
                    )}
                  </Table>
                </TableContainer>
              </Container>
            </>
          {selectedIncome && selectedIncomeIndex !== null && (
            <IncomeEditModal
              open={isEditModalOpen}
              onClose={handleEditModalClose}
              incomeData={{ ...selectedIncome, index: selectedIncomeIndex }}
            />
          )}
        </Grid>
      </div>
    </>
  );
};

export default IncomeDetails;
